package fr.ulille.iut.pizzaland.dao;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id INTEGER PRIMARY KEY, name VARCHAR UNIQUE NOT NULL, prixpetite REAL NOT NULL, prixgrande REAL NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzaingredients (pizza INTEGER, ingredient INTEGER, CONSTRAINT fk_pizza FOREIGN KEY(pizza) REFERENCES pizza(id), CONSTRAINT fk_ingredient FOREIGN KEY(ingredient) REFERENCES ingredient(id))")
	void createAssociationTable();

	@Transaction
	default void createTable() {
		createPizzaTable();
		createAssociationTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS pizzas")
	void dropPizzaTable();

	@SqlUpdate("DROP TABLE IF EXISTS pizzaingredients")
	void dropAssociationTable();

	default void dropTable() {
		dropAssociationTable();
		dropPizzaTable();
	}

	@SqlUpdate("INSERT INTO pizzas (name, prixpetite, prixgrande) VALUES (:name, :prixpetite, :prixgrande)")
	@GetGeneratedKeys
	long insertPizza(String name, double prixpetite, double prixgrande);

	@SqlUpdate("INSERT INTO pizzaingredients (pizza, ingredient) VALUES (:idpizza, :idingredient)")
	@GetGeneratedKeys
	long insertAssociations(long idpizza, long idingredient);

	default long insertNewPizza(String name, double prixpetite, double prixgrande, Long[] ingredients) {
		long idpizza = insertPizza(name, prixpetite, prixgrande);
		for (long idingredient : ingredients) {
			insertAssociations(idpizza, idingredient);
		}
		return idpizza;
	}

	@SqlQuery("SELECT * FROM pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAll();

	@SqlQuery("SELECT * FROM pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findById(long id);

	@SqlQuery("SELECT * FROM pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByName(String name);

	@SqlUpdate("DELETE FROM pizzas WHERE id = :id")
	void remove(long id);

}
