package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private long id;
	private String name;
	private double prixpetite;
	private double prixgrande;
	private Long[] ingredients;

	public Pizza() {
	}

	public Pizza(long id, String name, double prixpetite, double prixgrande, Long[] ingredients) {
		this.id = id;
		this.name = name;
		this.prixpetite = prixpetite;
		this.prixgrande = prixgrande;
		this.ingredients = ingredients;
	}

	public void setId(long id) {
		this.id = id;
	}	

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrixpetite() {
		return prixpetite;
	}

	public void setPrixpetite(double prixpetite) {
		this.prixpetite = prixpetite;
	}

	public double getPrixgrande() {
		return prixgrande;
	}

	public void setPrixgrande(double prixgrande) {
		this.prixgrande = prixgrande;
	}

	public Long[] getIngredients() {
		return ingredients;
	}

	public void setIngredients(Long[] ingredients) {
		this.ingredients = ingredients;
	}

	public static PizzaDto toDto(Pizza p) {
		PizzaDto dto = new PizzaDto();
		dto.setId(p.getId());
		dto.setName(p.getName());
		dto.setPrixpetite(p.getPrixpetite());
		dto.setPrixgrande(p.getPrixgrande());
		dto.setIngredients(p.getIngredients());

		return dto;
	}

	public static Pizza fromDto(PizzaDto dto) {
		Pizza pizza = new Pizza();
		pizza.setId(dto.getId());
		pizza.setName(dto.getName());
		pizza.setPrixpetite(dto.getPrixpetite());
		pizza.setPrixgrande(dto.getPrixgrande());
		pizza.setIngredients(dto.getIngredients());

		return pizza;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id != other.id)
			return false;
		if (ingredients == null) {
			if (other.ingredients != null)
				return false;
		} else if (!ingredients.equals(other.ingredients))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (Double.doubleToLongBits(prixgrande) != Double.doubleToLongBits(other.prixgrande))
			return false;
		if (Double.doubleToLongBits(prixpetite) != Double.doubleToLongBits(other.prixpetite))
			return false;
		return true;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());
		dto.setPrixpetite(pizza.getPrixpetite());
		dto.setPrixgrande(pizza.getPrixgrande());
		dto.setIngredients(pizza.getIngredients());

		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());
		pizza.setPrixpetite(dto.getPrixpetite());
		pizza.setPrixgrande(dto.getPrixgrande());
		pizza.setIngredients(dto.getIngredients());

		return pizza;
	}

	@Override
	public String toString() {
		return "Pizza [id=" + id + ", name=" + name + "prixpetite" + prixpetite + "prixgrande" + prixgrande
				+ "ingredients" + ingredients.toString() + "]";
	}
}
